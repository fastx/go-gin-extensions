package requestid

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

const (
	DefaultRequestIdHeader = "X-Request-Id"
	DefaultMetaDataKey = "RequestId"
)

type IdGeneratorFunc func() string

type Config struct {
	// Renew request id no matter already has value or not
	Renew bool
	// Header for request id store
	Header string
	// Key for context's metadata
	Key string
	// Set custom id generator function
	Func IdGeneratorFunc
}

// Default id generator function
func defaultIdGenerator() string {
	return uuid.New().String()
}

func New(config Config) gin.HandlerFunc {
	header := DefaultRequestIdHeader
	key := DefaultMetaDataKey
	fn := defaultIdGenerator

	if len(config.Header) != 0 {
		header = config.Header
	}

	if len(config.Key) != 0 {
		key = config.Key
	}

	if config.Func != nil {
		fn = config.Func
	}

	return func(c *gin.Context) {
		// Check for incoming header, use it if exists
		requestID := c.Request.Header.Get(header)

		if config.Renew || requestID == "" {
			requestID = fn()
		}

		// Expose it for use in the application
		c.Set(key, requestID)

		// Set header value
		c.Writer.Header().Set(header, requestID)
		c.Next()
	}
}
