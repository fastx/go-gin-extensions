module gitlab.com/fastx/go-gin-extensions

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/google/uuid v1.1.1
)
